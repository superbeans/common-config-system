package com.superbeans.cms.mvc.controller.views;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("")
public class HelloController {

	@RequestMapping({"/index", ""})
	public ModelAndView indexView() {
		ModelAndView view = new ModelAndView("index");
		return view;
	}
}
